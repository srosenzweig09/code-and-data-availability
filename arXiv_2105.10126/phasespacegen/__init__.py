import numpy as np

class Particle:
	def __init__(self, mass, energy, dir):
		# Shapes:
		#   mass: (-1, 1) or float
		#   energy: (-1, 1) or float
		#   dir: (-1, 3) or (3,)
		# dir will be normalized
		
		self.mass = np.reshape(mass, (-1, 1))
		self.energy = np.reshape(energy, (-1, 1))
		self.dir = np.reshape(dir, (-1, 3))
		
		self.dir /= np.linalg.norm(self.dir, axis=-1, keepdims=True)
		self.momentum = (np.maximum(0, self.energy**2 - self.mass**2))**.5
		self.p_3vec = self.momentum * self.dir
		self.energy = np.broadcast_to(self.energy, (self.p_3vec.shape[0], 1))
		self.p_4vec = np.concatenate([self.energy, self.p_3vec], axis=-1)
		
	def boost(self, v, dir, gamma=None):
		def dot(a, b):
			return np.sum(a*b, axis=-1, keepdims=True)
		
		if gamma is None:
			gamma = (1-v**2)**-.5
		v_3vec = v*dir
				
		energy_new = gamma * (self.energy + dot(self.p_3vec, v_3vec))
		p_3vec_new = self.p_3vec + (gamma-1) * dot(self.p_3vec, dir) * dir + gamma*self.energy*v_3vec
				
		dir_new = p_3vec_new / np.linalg.norm(p_3vec_new, axis=-1, keepdims=True)
		
		return Particle(self.mass, energy_new, dir_new)
	
	def decay(self, m1, m2, rng=None, m1_theta_com=None):
		m1 = np.reshape(m1, (-1, 1))
		m2 = np.reshape(m2, (-1, 1))
		N = max(
			len(self.mass), len(self.energy), len(self.dir),
			len(m1), len(m2)
		)
		
		if m1_theta_com is not None:
			m1_theta_com = np.reshape(m1_theta_com, (-1, 1))
			N = max(N, len(m1_theta_com))
		else:
			m1_theta_com = np.arccos(
				rng.uniform(-1, 1, size=(N, 1))
			)
		
		e1_com = (self.mass**2 + m1**2 - m2**2)/(2*self.mass)
		e2_com = (self.mass**2 + m2**2 - m1**2)/(2*self.mass)
		
		unnorm_random_dir = rng.standard_normal(size=(N, 3))
		perp_dir = unnorm_random_dir - unnorm_random_dir*self.dir
		perp_dir /= np.linalg.norm(perp_dir, axis=-1, keepdims=True)
		
		dir1_com = self.dir*np.cos(m1_theta_com) + \
					perp_dir*np.sin(m1_theta_com)
		
		dir2_com = -dir1_com
		
		child1_com = Particle(m1, e1_com, dir1_com)
		child2_com = Particle(m2, e2_com, dir2_com)
		
		boost_v = (np.maximum(0, self.energy**2 - self.mass**2))**.5 / self.energy
		boost_gamma = self.energy/self.mass
		child1 = child1_com.boost(boost_v, self.dir, boost_gamma)
		child2 = child2_com.boost(boost_v, self.dir, boost_gamma)
		
		return child1, child2